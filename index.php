<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>

    <title>Zefro</title>
</head>

<body>

    <div class="side-bar">
        <div class="side-bar-logo">

        </div>

        <div id="btn-arrow-active" class="btn-arrow">
            <i class='fas fa-angle-double-right arrow-center'></i>
        </div>

    </div>

    <div class="main-page">
        <div class="overlay">
        </div>
        <div class="header-section">
            <div class="logo">Jao Design</div>
            <ul class="links">
                <li><a href="#" class="active">Profile</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Skills</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </div>

        <div class="introduction-section">
            <h1> <span>Welcome !</span> How are you?</h1>
            <p>a book or other written or printed work, regarded in terms of its content rather than its physical form.</p>
        </div>



    </div>


    <!-- About us Start -->
    <div class="container">
        <div class="about-us-section">
            <div class="info">
                <h2>About Us</h2>
                <p>Website sentence examples. Maybe there's an 'I hate Jackson Parrish' website out there. The military doesn't buy their haircuts, website design, or piano lessons. A website called Wolfram Alpha is amazing to me, especially in its aspirations.</p>
            </div>
            <div class="image">
                <img src="./assets/img/05.jpg" alt="">
            </div>
        </div>
    </div>

    <!-- About us End -->

    <!-- Gallery  Start -->
    <div class="gallery">
        <div class="container">
            <h2>Gallery</h2>
            <div class="image-box">
                <img src="assets/img/01.jpg" alt="Desc1">
                <img src="assets/img/02.jpg" alt="Desc2">
                <img src="assets/img/03.jpg" alt="Desc3">
                <img src="assets/img/04.jpg" alt="Desc4">
                <img src="assets/img/05.jpg" alt="Desc5">
            </div>

        </div>
    </div>

    <!-- Gallery  End -->

    <!-- Timeline  Start -->
    <div class="timeline">
        <div class="container">
            <div class="timeline-content">
                <div class="timeline-year">2010</div>

                <div class="left">
                    <div class="content">
                        <h3>TES HEADING</h3>
                        <p>Any sentence examples. Alex had invited her to look at his financial files any time she wanted, and yet it seemed an intrusion on his privacy. Will we see any dolphins, Dad? But this wasn't just any trip.</p>
                    </div>
                </div>
                <div class="clear-fix">
                    <div class="right">
                        <div class="content">
                            <h3>TES HEADING</h3>
                            <p>Any sentence examples. Alex had invited her to look at his financial files any time she wanted, and yet it seemed an intrusion on his privacy. Will we see any dolphins, Dad? But this wasn't just any trip.</p>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <!-- Timeline  End -->



</body>

</html>
<script src="assets/js/touchdetection.js"></script>
<script src="assets/js/main.js"></script>