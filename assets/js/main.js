//select landing page element

let landingPage = document.querySelector(".main-page");

//get the array of imgs
let imageArr = ["01.jpg", "02.jpg", "03.jpg", "04.jpg", "05.jpg"];

//random number for changing the landing page

setInterval(() => {
    let randnum = Math.floor(Math.random() * imageArr.length);
    void landingPage.offsetWidth;

    landingPage.style.backgroundImage = 'url("../portfolio/assets/img/' + imageArr[randnum] + '")';
    // setInterval(() => {
    //     landingPage.classList.add("o_tip");
    // }, 1000);
}, 3000);


//for sidebar
let sidebar = document.querySelector(".side-bar");
let btnArrow = document.querySelector(".btn-arrow");


let fas = document.querySelector(".fas");

document.body.addEventListener("mousemove", () => {
    var x = event.clientX;
    var y = event.clientY;

    if (x < 50) {

        sidebar.classList.add("btn-arrow-show");
    } else {
        sidebar.classList.remove("btn-arrow-show");
    }
});

btnArrow.addEventListener("click", () => {
    let btnArrowId = document.querySelector("#btn-arrow-active").classList.length;
    if (btnArrowId == 1) {
        sidebar.classList.add("open");
        fas.classList.remove("fa-angle-double-right");
        fas.classList.add("fa-angle-double-left");
        btnArrow.classList.add("btn-arrow-active");
    }
    if (btnArrowId == 2) {
        sidebar.classList.remove("open");
        fas.classList.remove("fa-angle-double-left");
        fas.classList.add("fa-angle-double-right");
        btnArrow.classList.remove("btn-arrow-active");
    }

});

let gallery = document.querySelectorAll(".gallery img");

gallery.forEach(img => {
    img.addEventListener('click', (e) => {
        //create overlay element
        let overlayGallery = document.createElement("div");
        // add class to overlay
        overlayGallery.className = 'popup-overlay';
        //append Overlay to the body
        document.body.appendChild(overlayGallery);

        //create the popup box
        let popup = document.createElement("div");
        //add class to popup box
        popup.className = 'popup-box';
        //get the image desc 
        if (img.alt !== null) {
            //create heading
            let imgHeading = document.createElement("h3");
            //create 
            let imgHeadingText = document.createTextNode(img.alt);
            imgHeading.appendChild(imgHeadingText);
            popup.appendChild(imgHeading);
        }
        // create the image
        let popupImage = document.createElement("img");
        //set image src
        popupImage.src = img.src;

        // add image to popup box
        popup.appendChild(popupImage);
        //add to the body
        document.body.appendChild(popup);

        //create span
        let closeButton = document.createElement("span");

        //create textnode
        let closeButtonText = document.createTextNode("x");

        //append the text to button
        closeButton.appendChild(closeButtonText);
        // get the attribute of close button
        closeButton.className = 'close-button';
        //adding to the popup
        popup.appendChild(closeButton);

        let closeButtonleft = document.createElement("span");

        // get the attribute of close button
        closeButtonleft.className = 'btn-img-left fa fa-angle-left';
        //adding to the popup
        popup.appendChild(closeButtonleft);

        let closeButtonRight = document.createElement("span");

        // get the attribute of close button
        closeButtonRight.className = 'btn-img-right fa fa-angle-right';
        //adding to the popup
        popup.appendChild(closeButtonRight);

    });

    document.addEventListener('click', (e) => {
        if (e.target.className == 'close-button') {
            //remove the popup in document
            e.target.parentNode.remove();
            //remove overlay popup
            document.querySelector(".popup-overlay").remove();
        }

    });
});