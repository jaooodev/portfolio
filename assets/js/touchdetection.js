 window.addEventListener('load', function() {
         let sidebar = document.querySelector(".side-bar");
         let fas = document.querySelector(".fas");
         var touchsurface = document.querySelector('body'),
             startX,
             startY,
             dist,
             threshold = 150, //required min distance traveled to be considered swipe
             allowedTime = 200, // maximum time allowed to travel that distance
             elapsedTime,
             startTime

         function handleswipe(isrightswipe) {
             if (isrightswipe) {
                 sidebar.classList.add("open");
                 fas.classList.remove("fa-angle-double-right");
                 fas.classList.add("fa-angle-double-left");
             } else {
                 sidebar.classList.remove("open");
                 fas.classList.remove("fa-angle-double-left");
                 fas.classList.add("fa-angle-double-right");

             }
         }

         touchsurface.addEventListener('touchstart', function(e) {

             var touchobj = e.changedTouches[0]
             dist = 0
             startX = touchobj.pageX
             startY = touchobj.pageY
             startTime = new Date().getTime() // record time when finger first makes contact with surface
             e.preventDefault()
         }, false)

         touchsurface.addEventListener('touchmove', function(e) {
             e.preventDefault() // prevent scrolling when inside DIV
         }, false)

         touchsurface.addEventListener('touchend', function(e) {
             var touchobj = e.changedTouches[0]
             dist = touchobj.pageX - startX // get total dist traveled by finger while in contact with surface
             elapsedTime = new Date().getTime() - startTime // get time elapsed
                 // check that elapsed time is within specified, horizontal dist traveled >= threshold, and vertical dist traveled <= 100
             var swiperightBol = (elapsedTime <= allowedTime && dist >= threshold && Math.abs(touchobj.pageY - startY) <= 100)
             handleswipe(swiperightBol)
             e.preventDefault()
         }, false)

     }, false) // end window.onload